import 'dart:convert';
import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _toDoController = TextEditingController();

  List _toDoList = [];

  Map<String, dynamic> _lastRemoved;
  int _lastRemovedPos;

  @override
  void initState() {
    //Estado inicial da minha tela
    // TODO: implement initState
    super.initState();

    _readData().then((data) {
      setState(() {
        _toDoList = json.decode(data);
      });
    });
  }

  void _toDoAdd() {
    setState(() {
      Map<String, dynamic> newTask = Map();
      newTask["title"] = _toDoController.text;
      newTask["status"] = false;
      _toDoController.text = "";
      _toDoList.add(newTask);

      _saveData();
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _toDoList.sort((a, b) {
          if (a["status"] && !b["status"])
            return 1;
          else if (!a["status"] && b["status"])
            return -1;
          else
            return 0;
        });
        _saveData();
      });
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Minhas Tarefas"),
          backgroundColor: Colors.deepPurple[700],
          centerTitle: true),
      body: bodyApp(),
    );
  }

  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/data.json");
  }

  Future<File> _saveData() async {
    String data = json.encode(_toDoList);

    final file = await _getFile();
    return file.writeAsString(data);
  }

  Future<String> _readData() async {
    try {
      final file = await _getFile();

      return file.readAsString();
    } catch (e) {
      return null;
    }
  }

  Widget bodyApp() {
    return Column(children: <Widget>[
      Container(
        padding: EdgeInsets.fromLTRB(17.0, 1.0, 7.0, 1.0),
        child: Row(
          children: <Widget>[
            Expanded(
                child: TextField(
              controller: _toDoController,
              decoration: InputDecoration(
                  labelText: 'Nova Tarefa',
                  labelStyle: TextStyle(color: Colors.deepPurple)),
            )),
            RaisedButton(
              color: Colors.deepPurple[700],
              child: Text("ADD"),
              textColor: Colors.white,
              onPressed: _toDoAdd,
            )
          ],
        ),
      ),
      Expanded(
          child: RefreshIndicator(
              child: ListView.builder(
                  padding: EdgeInsets.only(top: 10),
                  itemCount: _toDoList.length,
                  itemBuilder: buildItem),
              onRefresh: _refresh))
    ]);
  }

  Widget buildItem(BuildContext context, int index) {
    return Dismissible(
      key: Key(index.toString()),
      background: Container(
        color: Colors.red,
        child: Align(
            alignment: Alignment(-0.9, 0.0),
            child: Icon(Icons.delete_sweep, color: Colors.white)),
      ),
      direction: DismissDirection.startToEnd,
      child: CheckboxListTile(
        title: Text(_toDoList[index]["title"]),
        value: _toDoList[index]["status"],
        secondary: CircleAvatar(
          child: Icon(_toDoList[index]["status"] ? Icons.check : Icons.cancel),
          backgroundColor: Colors.deepPurple[700],
        ),
        onChanged: (bool value) {
          setState(() {
            _toDoList[index]["status"] = value;
            _saveData();
          });
        },
      ),
      onDismissed: (direction) {
        setState(() {
          //fazendo backup da task removida
          _lastRemoved = Map.from(_toDoList[index]);

          //salvando posição da task removida
          _lastRemovedPos = index;

          //removendo da lista
          _toDoList.removeAt(index);

          //salvando
          _saveData();

          final snack = SnackBar(
            content: Text("Tarefa \"${_lastRemoved["title"]}\" removida"),
            backgroundColor: Colors.black,
            action: SnackBarAction(
                label: "Desfazer",
                onPressed: () {
                  setState(() {
                    //adicionando novamente a task deletada na mesma posição que ela estava
                    _toDoList.insert(_lastRemovedPos, _lastRemoved);

                    _saveData();
                  });
                }),
            duration: Duration(seconds: 5),
          );

          Scaffold.of(context).removeCurrentSnackBar(); //Para evitar bugs, remover snack bar antes de mostrar a nova
          Scaffold.of(context).showSnackBar(snack);
        });
      },
    );
  }
}
